
from django.urls import path

from news import views

urlpatterns = [
    path('', views.index),
    path('filter-options', views.filter_options),
    path('news/<str:country>/<str:category>', views.query),
]
