# Generated by Django 3.0.4 on 2020-03-13 05:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.TextField()),
                ('category', models.TextField()),
                ('source_id', models.TextField(blank=True, null=True)),
                ('source_name', models.TextField(blank=True, null=True)),
                ('author', models.TextField(blank=True, null=True)),
                ('title', models.TextField()),
                ('description', models.TextField(blank=True, null=True)),
                ('url', models.URLField()),
                ('image_url', models.URLField(blank=True, null=True)),
                ('published_at', models.DateTimeField()),
                ('content', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Pooling',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.TextField()),
                ('category', models.TextField()),
                ('last_pooling', models.DateTimeField()),
            ],
            options={
                'unique_together': {('country', 'category')},
            },
        ),
        migrations.AddIndex(
            model_name='news',
            index=models.Index(fields=['country', 'category', 'published_at'], name='news_news_country_417fd0_idx'),
        ),
        migrations.AddIndex(
            model_name='news',
            index=models.Index(fields=['country'], name='news_news_country_cd9c01_idx'),
        ),
        migrations.AddIndex(
            model_name='news',
            index=models.Index(fields=['category'], name='news_news_categor_05ff13_idx'),
        ),
        migrations.AddIndex(
            model_name='news',
            index=models.Index(fields=['source_name'], name='news_news_source__5ecdd7_idx'),
        ),
        migrations.AddIndex(
            model_name='news',
            index=models.Index(fields=['author'], name='news_news_author_d7837c_idx'),
        ),
    ]
