from django.db import models

COUNTRIES = ('us', 'jp', 'tw', 'kr')
CATEGORY = ('business', 'general', 'health', 'science', 'technology')


class News(models.Model):
    class Meta:
        indexes = [
            models.Index(fields=['country', 'category', 'published_at']),
            models.Index(fields=['country']),
            models.Index(fields=['category']),
            models.Index(fields=['source_name']),
            models.Index(fields=['author']),
        ]

    country = models.TextField()
    category = models.TextField()
    source_id = models.TextField(null=True, blank=True)
    source_name = models.TextField(null=True, blank=True)
    author = models.TextField(null=True, blank=True)
    title = models.TextField()
    description = models.TextField(null=True, blank=True)
    url = models.URLField()
    image_url = models.URLField(null=True, blank=True)
    published_at = models.DateTimeField()
    content = models.TextField(null=True, blank=True)

    def to_json(self):
        return {
            'id': self.id,
            'source_id': self.source_id,
            'source_name': self.source_name,
            'title': self.title,
            'author': self.author,
            'description': self.description,
            'url': self.url,
            'image_url': self.image_url,
            'content': self.content,
            'published_at': self.published_at.strftime('%Y-%m-%d %H:%M:%S (UTC)'),
            't': self.published_at.timestamp()
        }


class Pooling(models.Model):
    class Meta:
        unique_together = [
            ['country', 'category']
        ]

    country = models.TextField()
    category = models.TextField()
    last_pooling = models.DateTimeField()
