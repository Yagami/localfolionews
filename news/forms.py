
from django.forms import ModelForm
from dateutil import parser

from .models import News


class NewsForm(ModelForm):
    class Meta:
        model = News
        exclude = []

    @classmethod
    def from_newsapi(cls, country: str, category: str, payload: dict):
        source = payload.pop('source')
        payload.update((('source_id', source['id']),
                        ('source_name', source['name']),
                        ('country', country),
                        ('category', category),
                        ('image_url', payload.pop('urlToImage', None)),
                        ('published_at', parser.parse(payload.pop('publishedAt')))
                        ))
        return cls(payload)
