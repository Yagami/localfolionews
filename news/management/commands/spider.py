
from functools import partial
from time import time, sleep
import logging
import requests

from django.core.management.base import BaseCommand
from django.utils import timezone
from django.db.models import Max

from news.models import COUNTRIES, CATEGORY, News, Pooling
from news.forms import NewsForm

logger = logging.getLogger('spider')


class Command(BaseCommand):
    help = 'Running Newsapi spider'
    apikey: str = None
    s: requests.Session = None

    def add_arguments(self, parser):
        parser.add_argument('apikey', type=str)

    def make_api_request(self, country: str, category: str):
        url = f'http://newsapi.org/v2/top-headlines?country={country}&category={category}&apiKey={self.apikey}'
        logger.debug('Request: %s', url)
        ret = self.s.get(url)
        if ret.ok:
            doc = ret.json()
            if doc['status'] == 'ok':
                return doc['articles']
        raise RuntimeError(f'Request error: {ret.status}/{ret.text}')

    def handle(self, *args, apikey, **kwargs):
        self.s = requests.Session()
        self.apikey = apikey
        self.run_forever()

    def run_forever(self):
        while True:
            try:
                for country in COUNTRIES:
                    for category in CATEGORY:
                        self.pull_news(country, category)
                sleep(200)
            except KeyboardInterrupt:
                raise
            except Exception:
                logger.exception('Unhandle Error')

    def pull_news(self, country: str, category: str):
        pooling: Pooling = Pooling.objects.filter(country=country, category=category).first()
        if pooling and time() - pooling.last_pooling.timestamp() < 600:
            logger.warning('Ignore %s/%s (last pooling=%s)', country, category, pooling.last_pooling)
            return

        anchor = News.objects.filter(country=country, category=category).aggregate(Max('published_at'))['published_at__max']
        ts = 0 if anchor is None else anchor.timestamp()

        logger.warning('Updating %s/%s (last pooling=%s)', country, category, pooling.last_pooling if pooling else None)
        cnt = 0
        for form in filter(lambda f: not f.errors,
                           map(partial(NewsForm.from_newsapi, country, category),
                               self.make_api_request(country, category))):
            if form.cleaned_data['published_at'].timestamp() > ts:
                form.save()
                cnt += 1
            else:
                break

        logger.warning(' -- %i insterted', cnt)
        Pooling.objects.update_or_create(country=country, category=category, defaults={'last_pooling': timezone.now()})
