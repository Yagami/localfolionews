from django.core.exceptions import SuspiciousOperation
from django.shortcuts import render
from django.utils.timezone import datetime, utc
from django.http import JsonResponse

from .models import News, COUNTRIES, CATEGORY


def index(request):
    return render(request, 'news/index.html')


def filter_options(request):
    return JsonResponse({'country': COUNTRIES, 'category': CATEGORY})


def query(request, country: str, category: str):
    t = request.GET.get('t')
    if t is None:
        q = News.objects.filter(country=country, category=category).order_by('-published_at')[:10]
    else:
        try:
            timestamp = datetime.fromtimestamp(float(t)).replace(tzinfo=utc)
        except (TypeError, ValueError):
            raise SuspiciousOperation('Invalid t')
        q = News.objects.filter(country=country, category=category, published_at__lt=timestamp).order_by('-published_at')[:10]

    return JsonResponse({'news': [e.to_json() for e in q]})
