# localfolionews

## Install Steps

1. Clone Project
2. Change working directory to project
3. Run `pip install -r requirements.txt`
4. Run `./manage.py migrate`

## Run

1. Launch background worker by `./manage.py spider {YOUR_NEWSAPI_KEY}`
2. Launch django project by `./manage.py runserver`
3. Open http://127.0.0.1:8000/

* There is no need to modify `settings.py`
